/**
 * Pizza delivery prompt example
 * run example by writing `node pizza.js` in your console
 */

"use strict";
var inquirer = require("inquirer");
const functionHelper = require("./functionHelper");
console.log("Hi, welcome to Basic Node Interactive Console Application");

var questions = [
  {
    type: "list",
    name: "type",
    message: "Choose which function to run",
    choices: ["Triangle", "Square", "Diamond", "Fibonacci"],
    filter: function(val) {
      return val.toLowerCase();
    }
  },
  {
    type: "input",
    name: "size",
    message: "How many do you need?",
    validate: function(value) {
      var valid = !isNaN(parseFloat(value));
      return valid || "Please enter a number";
    },
    filter: Number
  }
];

inquirer.prompt(questions).then(answers => {
  console.log("\nHeres the Command : ");
  console.log(JSON.stringify(answers, null, "  "));
  console.log("\nHeres the Result : ");
  functionHelper.runFunction(answers);
});
