# Node Command Line App Test

A simple node.js console/terminal application for generating a triangle, square and diamond shapes and fibonacci sequence 
### Prerequisites

You need to have node and npm installer to run this application


### Installing

after you clone the repository, just simply enter these command in the console

```
npm install
```

to start the application you need to run

```
npm start
```


## Built With

* [Inquirer](https://github.com/SBoudrias/Inquirer.js) - A collection of common interactive command line user interfaces.