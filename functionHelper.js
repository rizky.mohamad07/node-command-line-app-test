"use strict";

module.exports.runFunction = function(option) {
  switch (option.type) {
    case "triangle":
      GenerateTriangle(option.size);
      break;
    case "square":
      GenerateSquare(option.size);
      break;
    case "diamond":
      GenerateDiamond(option.size);
      break;
    case "fibonacci":
      GenerateFibonacci(option.size);
      break;
    default:
      console.log("Function command " + option.type + "is not found");
  }
  return;
};

function GenerateTriangle(size) {
    for (let i = 0; i <= size; i++) {
        let output = "";
          for (let j = 0; j < size - i; j++) {
            output += " ";
          }
          for (let k = 0; k < i; k++) {
            output += "*";
          }
        console.log(output);
      }
  return;
}

function GenerateSquare(size) {
    for (let i = 0; i < size; i++) {
        let output = "";
          for (let j = 0; j < size; j++) {
            if(i == 0 || i == (size -1)){
                output += "*";
            } else {
                if (j == 0 || j == (size -1)) {
                    output += "*";
                } else {
                    output += " ";
                }
            }
            
          }
        console.log(output);
      }
  return;
}

function GenerateDiamond(size) {
  //draw top side
  for (let i = 0; i <= size; i++) {
    let output = "";
    if (i % 2 != 0) {
      for (let j = 0; j < size - i; j++) {
        output += " ";
      }
      for (let k = 0; k < i; k++) {
        output += "* ";
      }
    console.log(output);
    }
  }
  //draw bottom side
  for (let i = size; i > 0; i--) {
    let output = "";
    if (i % 2 != 0) {
      for (let j = 0; j < size - i; j++) {
        output += " ";
      }
      for (let k = 0; k < i; k++) {
        output += "* ";
      }
      console.log(output);
    }
   
  }
  return;
}

var fibonacciSeries = function(max) {
  let fib = [1, 2];
  let i = 2;
  while (fib[i - 1] + fib[i - 2] <= max) {
    i = fib.push(fib[i - 1] + fib[i - 2]);
  }
  return fib;
};

function GenerateFibonacci(size) {
  //Generate Fibonacci Sequence using recursive function
  let fibonacciResult = fibonacciSeries(size);
  //Display the result in console
  fibonacciResult.forEach(element => {
    console.log(element);
  });
  return;
}
